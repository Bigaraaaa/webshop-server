package web_shop_basic;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;

@SpringBootApplication
public class WebShopBasic extends SpringBootServletInitializer {

	public static void main(String[] args) {
		SpringApplication.run(WebShopBasic.class, args);
	}

}
