package web_shop_basic.controllers;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import web_shop_basic.dto.AccountDataDto;
import web_shop_basic.models.AccountData;
import web_shop_basic.services.GenericService;

@RestController
@RequestMapping(path = "/account-data")
public class AccountDataController extends GenericController<AccountDataDto, AccountData, Long> {

	public AccountDataController(GenericService<AccountData, Long> service) {
		super(service, AccountDataDto.class);
		// TODO Auto-generated constructor stub
	}

}
