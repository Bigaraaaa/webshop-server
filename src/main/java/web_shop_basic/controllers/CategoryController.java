package web_shop_basic.controllers;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import web_shop_basic.dto.CategoryDto;
import web_shop_basic.models.Category;
import web_shop_basic.services.GenericService;

@RestController
@RequestMapping(path = "/category")
public class CategoryController extends GenericController<CategoryDto, Category, Long> {

	public CategoryController(GenericService<Category, Long> service) {
		super(service, CategoryDto.class);
		// TODO Auto-generated constructor stub
	}

}
