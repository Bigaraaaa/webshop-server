package web_shop_basic.controllers;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import web_shop_basic.dto.OrdersDto;
import web_shop_basic.models.Orders;
import web_shop_basic.models.Product;
import web_shop_basic.services.GenericService;
import web_shop_basic.services.OrdersService;

@RestController
@RequestMapping(path = "/orders")
public class OrdersController extends GenericController<OrdersDto, Orders, Long> {

	@Autowired
	private OrdersService ordersService;

	@Autowired
	private ModelMapper modelMapper;

	public OrdersController(GenericService<Orders, Long> service) {
		super(service, OrdersDto.class);
		// TODO Auto-generated constructor stub
	}

	// @RequestMapping(value="/add-to-cart", method=RequestMethod.POST)
	// public ResponseEntity<OrdersDto> add(@RequestBody Orders orders, Product
	// product) {
	// System.out.println("---");
	//// ordersService.addToCart(orders, product);
	// System.out.println("---");
	// return new ResponseEntity<OrdersDto>(modelMapper.map(orders,
	// OrdersDto.class), HttpStatus.CREATED);
	// }

	@RequestMapping(value = "/fulfillOrder/{id}", method = RequestMethod.PUT)
	public ResponseEntity<OrdersDto> saveOrdersProduct(@PathVariable Long id) {
		System.out.println("----------");
		Orders o = ordersService.fullfillOrder(id);
		return new ResponseEntity<OrdersDto>(modelMapper.map(o, OrdersDto.class), HttpStatus.CREATED);
	}

}
