package web_shop_basic.controllers;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.jaxb.SpringDataJaxb.OrderDto;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import web_shop_basic.dto.OrdersProductDto;
import web_shop_basic.dto.ProductDto;
import web_shop_basic.models.OrdersProduct;
import web_shop_basic.models.Product;
import web_shop_basic.models.ProductDetails;
import web_shop_basic.services.GenericService;
import web_shop_basic.services.OrdersProductService;

@RestController
@RequestMapping(path = "orders-product")
public class OrdersProductController extends GenericController<OrdersProductDto, OrdersProduct, Long> {
	
	@Autowired
	private ModelMapper modelMapper;

	@Autowired
	private OrdersProductService orderProdSer;
	
	public OrdersProductController(GenericService<OrdersProduct, Long> service) {
		super(service, OrdersProductDto.class);
		// TODO Auto-generated constructor stub
	}
	
	
	@RequestMapping(value = "/saveProduct/{id}", method = RequestMethod.POST)
	public ResponseEntity<OrdersProductDto> saveOrdersProduct(@PathVariable Long id, @RequestBody OrdersProduct ordersProduct) {
		System.out.println("----------");
		orderProdSer.save(id, ordersProduct);
		OrdersProductDto p = modelMapper.map(ordersProduct, OrdersProductDto.class);
		return new ResponseEntity<OrdersProductDto>(p, HttpStatus.CREATED);
	}

	
}
