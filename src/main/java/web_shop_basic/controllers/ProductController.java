package web_shop_basic.controllers;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import web_shop_basic.dto.ProductDto;
import web_shop_basic.models.Product;
import web_shop_basic.models.ProductDetails;
import web_shop_basic.services.FileService;
import web_shop_basic.services.GenericService;
import web_shop_basic.services.ProductService;

@RestController
@RequestMapping(path = "/product")
public class ProductController extends GenericController<ProductDto, Product, Long> {
	
	@Autowired
	private ProductService productService;
	
	@Autowired
	private FileService fileService;
	
	@Autowired
	private ModelMapper modelMapper;

	public ProductController(GenericService<Product, Long> service) {
		super(service, ProductDto.class);
		// TODO Auto-generated constructor stub
	}

	@RequestMapping(value = "/save-product", method = RequestMethod.POST)
	public ResponseEntity<ProductDto> saveProduct(@RequestBody ProductDetails productDetails) {
		Product pr = new Product();
		productService.saveProduct(pr, productDetails);
		ProductDto p = modelMapper.map(pr, ProductDto.class);
		return new ResponseEntity<ProductDto>(p, HttpStatus.CREATED);
	}
	


}
