package web_shop_basic.controllers;

import java.io.IOException;
import java.util.Optional;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.fasterxml.jackson.databind.ObjectMapper;

import web_shop_basic.dto.ProductDetailsDto;
import web_shop_basic.models.ProductDetails;
import web_shop_basic.services.FileService;
import web_shop_basic.services.GenericService;
import web_shop_basic.services.ProductDetailsService;

@RestController
@RequestMapping(path = "/product-details")
public class ProductDetailsController extends GenericController<ProductDetailsDto, ProductDetails, Long> {

	@Autowired
	private ProductDetailsService productDetailsService;
	
	@Autowired

	private ModelMapper modelMapper;
	
	@Autowired
	private FileService fileService;
	
	public ProductDetailsController(GenericService<ProductDetails, Long> service) {
		super(service, ProductDetailsDto.class);
		// TODO Auto-generated constructor stub
	}
	
	@RequestMapping(value = "/save-product", method = RequestMethod.POST, consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
	public ResponseEntity<ProductDetails> saveProductDetails(@RequestPart("productImage") Optional<MultipartFile> file, @RequestPart("data") String productString) throws IOException {
		ProductDetails product = new ObjectMapper().readValue(productString, ProductDetails.class);
		if(file.isPresent()) {
			fileService.saveProductImage(file.get(), "product_" + product.getName(), product);
		}
		productDetailsService.add(product);
		return new ResponseEntity<ProductDetails>(product, HttpStatus.CREATED);
	}
	
	@RequestMapping(value="/edit/{id}", method=RequestMethod.PUT, consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    public ResponseEntity<ProductDetails> editProductDetails(@PathVariable Long id, @RequestPart("productImage") Optional<MultipartFile> file, @RequestPart("data") String productString) throws IOException {
		ProductDetails product = new ObjectMapper().readValue(productString, ProductDetails.class);
		if(file.isPresent()) {
			fileService.saveProductImage(file.get(), "product_" + product.getName(), product);
		}
		productDetailsService.add(product);
		return new ResponseEntity<ProductDetails>(product, HttpStatus.CREATED);
    }

}
