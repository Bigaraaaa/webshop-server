package web_shop_basic.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import web_shop_basic.dto.SizeDto;
import web_shop_basic.models.ProductDetails;
import web_shop_basic.models.Size;
import web_shop_basic.services.GenericService;
import web_shop_basic.services.SizeService;

@RestController
@RequestMapping(path = "/size")
public class SizeController extends GenericController<SizeDto, Size, Long> {

	@Autowired
	private SizeService sizeService;

	public SizeController(GenericService<Size, Long> service) {
		super(service, SizeDto.class);
		// TODO Auto-generated constructor stub
	}

	@RequestMapping(value = "/addSize/{id}", method = RequestMethod.POST)
	public ResponseEntity<SizeDto> saveSize(@PathVariable Long id, @RequestBody Size size) {
		sizeService.saveSize(id, size);
		return new ResponseEntity<SizeDto>(HttpStatus.CREATED);
	}

}
