package web_shop_basic.controllers;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import web_shop_basic.dto.VisitorDto;
import web_shop_basic.models.Visitor;
import web_shop_basic.services.GenericService;

@RestController
@RequestMapping(path = "/visitor")
public class VisitorController extends GenericController<VisitorDto, Visitor, Long> {

	public VisitorController(GenericService<Visitor, Long> service) {
		super(service, VisitorDto.class);
		// TODO Auto-generated constructor stub
	}

}
