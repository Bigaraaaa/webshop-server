package web_shop_basic.dto;

public class AdministratorDto extends AbstractDto {

	private AccountDataDto accountData;
	
	public AdministratorDto() {
		
	}

	public AccountDataDto getAccountData() {
		return accountData;
	}

	public void setAccountData(AccountDataDto accountData) {
		this.accountData = accountData;
	}
	
	
}
