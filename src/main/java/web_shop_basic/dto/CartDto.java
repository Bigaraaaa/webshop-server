package web_shop_basic.dto;

public class CartDto extends AbstractDto {

	private ProductDto product;
	
	public CartDto() {
		
	}

	public ProductDto getProduct() {
		return product;
	}

	public void setProduct(ProductDto product) {
		this.product = product;
	}
	
	
}
