package web_shop_basic.dto;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class ProductDetailsDto extends AbstractDto {

	private String description;
	private String name;
	private String imageUrl;
	private LocalDate fromDate;
	private LocalDate toDate;
	private String categoryName;
	private List<SizeDto> size = new ArrayList<SizeDto>();

	public ProductDetailsDto() {
		
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getImageUrl() {
		return imageUrl;
	}

	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}

	public String getCategoryName() {
		return categoryName;
	}

	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}

	public LocalDate getFromDate() {
		return fromDate;
	}

	public void setFromDate(LocalDate fromDate) {
		this.fromDate = fromDate;
	}

	public LocalDate getToDate() {
		return toDate;
	}

	public void setToDate(LocalDate toDate) {
		this.toDate = toDate;
	}

	public List<SizeDto> getSize() {
		return size;
	}

	public void setSize(List<SizeDto> size) {
		this.size = size;
	}

	
	
	
}
