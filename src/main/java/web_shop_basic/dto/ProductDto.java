package web_shop_basic.dto;

import java.util.ArrayList;
import java.util.List;

public class ProductDto extends AbstractDto {


	private List<ProductDetailsDto> productDetails = new ArrayList<ProductDetailsDto>();
	private CategoryDto category;
	
	public ProductDto() {
		
	}

	public List<ProductDetailsDto> getProductDetails() {
		return productDetails;
	}

	public void setProductDetails(List<ProductDetailsDto> productDetails) {
		this.productDetails = productDetails;
	}

	public CategoryDto getCategory() {
		return category;
	}

	public void setCategory(CategoryDto category) {
		this.category = category;
	}

	
}
