package web_shop_basic.dto;

public class SizeDto extends AbstractDto {

	private String amount;
	private Double price;
	private String type;

	public SizeDto() {

	}

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	public Double getPrice() {
		return price;
	}

	public void setPrice(Double price) {
		this.price = price;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

}
