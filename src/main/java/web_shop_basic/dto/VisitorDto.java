package web_shop_basic.dto;

import java.util.ArrayList;
import java.util.List;

public class VisitorDto extends AbstractDto {

	private AccountDataDto accountData;
	private List<OrdersDto> orders = new ArrayList<OrdersDto>();
	
	public VisitorDto() {
		
	}

	public AccountDataDto getAccountData() {
		return accountData;
	}

	public void setAccountData(AccountDataDto accountData) {
		this.accountData = accountData;
	}

	public List<OrdersDto> getOrders() {
		return orders;
	}

	public void setOrders(List<OrdersDto> orders) {
		this.orders = orders;
	}

	
	
}
