package web_shop_basic.models;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.validation.constraints.Size;

@Entity
public class AccountData implements Entities<AccountData, Long> {

	private static final long serialVersionUID = 737809047946902809L;
	
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	@Size(min = 2)
	private String name;
	@Size(min = 2)
	private String surname;
	@Column(unique = true, nullable = false)
	private String email;
	@Size(min = 8)
	private String password;
	@OneToMany(mappedBy = "accountData", cascade = {CascadeType.ALL})
	private Set<UserPermission> userPermission;
	
	@OneToMany(mappedBy = "accountData", cascade = {CascadeType.ALL})
	private Set<Administrator> administrator;
	
	@OneToMany(mappedBy = "accountData", cascade = {CascadeType.ALL})
	private Set<Visitor> visitor;
	
	public AccountData() {
		
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Set<UserPermission> getUserPermission() {
		return userPermission;
	}

	public void setUserPermission(Set<UserPermission> userPermission) {
		this.userPermission = userPermission;
	}

	public Set<Administrator> getAdministrator() {
		return administrator;
	}

	public void setAdministrator(Set<Administrator> administrator) {
		this.administrator = administrator;
	}

	public Set<Visitor> getVisitor() {
		return visitor;
	}

	public void setVisitor(Set<Visitor> visitor) {
		this.visitor = visitor;
	}
	
}
