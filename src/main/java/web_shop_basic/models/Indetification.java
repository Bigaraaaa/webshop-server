package web_shop_basic.models;

import java.io.Serializable;

public interface Indetification<ID> extends Serializable {

    public ID getId();

    public void setId(ID id);

}