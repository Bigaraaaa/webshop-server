package web_shop_basic.models;

import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.validation.constraints.NotNull;

@Entity
public class Orders implements Entities<Orders, Long> {

	private static final long serialVersionUID = 9216494693981307153L;
	
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	private String name;
	private String surname;
	private String phone;
	private String email;
	private String street;
	private String number;
	private String floor;
	private String apartmant;
	private String additionalInformation;
	private Double sum;
	@NotNull
	@Enumerated(EnumType.STRING)
	private OrderStatus status;
	@ManyToOne(cascade = CascadeType.ALL)
	private Visitor visitor;
	@ManyToOne(cascade = CascadeType.ALL)
	private Administrator administrator;
	@OneToMany(mappedBy = "orders")
	private List<OrdersProduct> ordersProduct;
	
	public Orders() {
		
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Double getSum() {
		return sum;
	}

	public void setSum(Double sum) {
		this.sum = sum;
	}

	public Visitor getVisitor() {
		return visitor;
	}

	public void setVisitor(Visitor visitor) {
		this.visitor = visitor;
	}

	public Administrator getAdministrator() {
		return administrator;
	}

	public void setAdministrator(Administrator administrator) {
		this.administrator = administrator;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getStreet() {
		return street;
	}

	public void setStreet(String street) {
		this.street = street;
	}

	public String getNumber() {
		return number;
	}

	public void setNumber(String number) {
		this.number = number;
	}

	public String getFloor() {
		return floor;
	}

	public void setFloor(String floor) {
		this.floor = floor;
	}

	public String getApartmant() {
		return apartmant;
	}

	public void setApartmant(String apartmant) {
		this.apartmant = apartmant;
	}

	public String getAdditionalInformation() {
		return additionalInformation;
	}

	public void setAdditionalInformation(String additionalInformation) {
		this.additionalInformation = additionalInformation;
	}

	public OrderStatus getStatus() {
		return status;
	}

	public void setStatus(OrderStatus status) {
		this.status = status;
	}

	public List<OrdersProduct> getOrdersProduct() {
		return ordersProduct;
	}

	public void setOrdersProduct(List<OrdersProduct> ordersProduct) {
		this.ordersProduct = ordersProduct;
	}
	
	

	
	
}
