package web_shop_basic.repositories;

import java.util.Optional;

import org.springframework.stereotype.Repository;

import web_shop_basic.models.AccountData;

@Repository
public interface AccountDataRepository extends GenericRepository<AccountData, Long> {

	Optional<AccountData> findByEmail (String email);
	AccountData getByEmail (String email);
	Boolean existsByEmail(String email);
}
