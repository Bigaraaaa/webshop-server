package web_shop_basic.repositories;

import org.springframework.stereotype.Repository;

import web_shop_basic.models.Category;

@Repository
public interface CategoryRepository extends GenericRepository<Category, Long> {

}
