package web_shop_basic.repositories;

import java.io.Serializable;

import org.springframework.data.repository.NoRepositoryBean;
import org.springframework.data.repository.PagingAndSortingRepository;

import web_shop_basic.models.Entities;

@NoRepositoryBean
public interface GenericRepository<T extends Entities<T, ID>, ID extends Serializable> extends PagingAndSortingRepository<T, ID>{

   
  
}