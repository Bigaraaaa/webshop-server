package web_shop_basic.repositories;

import org.springframework.stereotype.Repository;

import web_shop_basic.models.OrdersProduct;

@Repository
public interface OrdersProductRepository extends GenericRepository<OrdersProduct, Long> {

}
