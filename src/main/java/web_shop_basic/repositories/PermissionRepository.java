package web_shop_basic.repositories;

import java.util.Optional;

import org.springframework.stereotype.Repository;

import web_shop_basic.models.Permission;

@Repository
public interface PermissionRepository extends GenericRepository<Permission, Long> {

	Optional<Permission> findByAuthority(String authority);
}
