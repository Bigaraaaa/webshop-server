package web_shop_basic.repositories;

import org.springframework.stereotype.Repository;

import web_shop_basic.models.ProductDetails;

@Repository
public interface ProductDetailsRepository extends GenericRepository<ProductDetails, Long> {

}
