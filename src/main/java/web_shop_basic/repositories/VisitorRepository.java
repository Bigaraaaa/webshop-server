package web_shop_basic.repositories;

import org.springframework.stereotype.Repository;

import web_shop_basic.models.Visitor;

@Repository
public interface VisitorRepository extends GenericRepository<Visitor, Long> {

}
