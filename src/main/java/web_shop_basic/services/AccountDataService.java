package web_shop_basic.services;

import java.util.HashSet;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import web_shop_basic.models.AccountData;
import web_shop_basic.models.UserPermission;
import web_shop_basic.repositories.AccountDataRepository;
import web_shop_basic.repositories.PermissionRepository;

@Service
public class AccountDataService extends GenericService<AccountData, Long> {

	@Autowired
	private AccountDataRepository accountDataRepository;
	
	@Autowired
	private PermissionRepository permissionRepository;
	
	@Autowired
	private PasswordEncoder passwordEncoder;
	
	@Transactional
	@Override
	public AccountData add(AccountData accountData) {
		accountData.setPassword(passwordEncoder.encode(accountData.getPassword()));

		accountData = accountDataRepository.save(accountData);
		accountData.setUserPermission(new HashSet<UserPermission>());
		accountData.getUserPermission().add(new UserPermission(null, accountData, permissionRepository.findById(1l).get()));
		accountDataRepository.save(accountData);
		
		return super.add(accountData);
	}
}
