package web_shop_basic.services;

import java.util.HashSet;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import web_shop_basic.models.Administrator;
import web_shop_basic.models.UserPermission;
import web_shop_basic.repositories.AdministratorRepository;
import web_shop_basic.repositories.PermissionRepository;

@Service
public class AdministratorService extends GenericService<Administrator, Long> {
	
	@Autowired
	private AdministratorRepository administratorRepository;

	@Autowired
	private PermissionRepository permissionRepository;
	
	@Autowired
	private PasswordEncoder passwordEncoder;
	
	@Override
	public Administrator add(Administrator model) {
		model.getAccountData().setPassword(passwordEncoder.encode(model.getAccountData().getPassword()));
		model = administratorRepository.save(model);
		
		model.getAccountData().setUserPermission(new HashSet<UserPermission>());
		model.getAccountData().getUserPermission().add(new UserPermission(null, model.getAccountData(), permissionRepository.findById(1l).get()));
		administratorRepository.save(model);
		return super.add(model);
	}
}
