package web_shop_basic.services;

import java.util.Optional;

//import com.jayway.jsonpath.Option;
//import com.mysql.cj.x.protobuf.MysqlxCrud.Order;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import web_shop_basic.models.Orders;
import web_shop_basic.models.OrdersProduct;
import web_shop_basic.repositories.OrdersProductRepository;

@Service
public class OrdersProductService extends GenericService<OrdersProduct, Long> {
	
	@Autowired
	private OrdersService ordSer;

	@Autowired
	private OrdersProductRepository ordProdRepo;

	public void save(Long id, OrdersProduct product) {
		System.out.println("----------" + id);
		System.out.println(product);
		System.out.println(product.getName());
		Optional<Orders> lastOrder = ordSer.findById(id);
		if (lastOrder.isPresent()) {
			OrdersProduct op = new OrdersProduct();
			op.setOrders(lastOrder.get());
			op.setAmount(product.getAmount());
			op.setDescription(product.getDescription());
			// op.setFromDate();
			// op.setToDate(toDate);
			op.setImageUrl(product.getImageUrl());
			op.setName(product.getName());
			op.setPrice(product.getPrice());
			ordProdRepo.save(op);			
		}
	}

}
