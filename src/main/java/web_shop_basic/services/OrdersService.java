package web_shop_basic.services;

import java.util.HashSet;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import web_shop_basic.models.OrderStatus;
import web_shop_basic.models.Orders;
import web_shop_basic.repositories.OrdersRepository;
import web_shop_basic.repositories.ProductRepository;

@Service
public class OrdersService extends GenericService<Orders, Long> {

	@Autowired
	private OrdersRepository ordersRepository;

	// public Orders addToCart(Orders orders) {
	// System.out.println(orders);
	// return ordersRepository.save(orders);
	// }
	public Orders fullfillOrder(Long id) {
		Optional<Orders> o = ordersRepository.findById(id);
		if (o.isPresent()) {
			o.get().setStatus(OrderStatus.fulfilled);
			ordersRepository.save(o.get());
			return o.get();
		} else {
			return null;
		}
	}
}
