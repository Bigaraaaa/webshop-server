package web_shop_basic.services;

import org.springframework.stereotype.Service;

import web_shop_basic.models.ProductDetails;

@Service
public class ProductDetailsService extends GenericService<ProductDetails, Long> {

    @Override
    public ProductDetails add(ProductDetails model) {
        model.setImageUrl("assets/products/" + model.getName() + ".jpg");
        super.save(model);
        return model;
    }    

}
