package web_shop_basic.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import web_shop_basic.models.Product;
import web_shop_basic.models.ProductDetails;

@Service
public class ProductService extends GenericService<Product, Long> {

    @Autowired
    private ProductDetailsService productDetailsService;

    // @Transactional
    public void saveProduct(Product product, ProductDetails productDetails) {
        System.out.println(product);
        System.out.println(productDetails.getName());
        save(product);
        productDetails.setProduct(product);
        productDetailsService.add(productDetails);
    }

}
