package web_shop_basic.services;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import web_shop_basic.models.ProductDetails;
import web_shop_basic.models.Size;
import web_shop_basic.repositories.ProductDetailsRepository;
import web_shop_basic.repositories.SizeRepository;

@Service
public class SizeService extends GenericService<Size, Long> {

    @Autowired
    private ProductDetailsRepository prodDetRepo;

    @Autowired
    private SizeRepository sizeRepository;

    // public void saveSize(Long id, Size size) {
    // System.out.println("SizeService----");
    // System.out.println(id);
    // System.out.println(size.getPrice());

    public void saveSize(Long id, Size size) {
        System.out.println("SizeService----");
        System.out.println(id);
        System.out.println(size.getPrice());
        Optional<ProductDetails> lastProdDetOpt = prodDetRepo.findById(id);
        if (lastProdDetOpt.isPresent()) {
            System.out.println(lastProdDetOpt.get().getId());
            size.setProductDetails(lastProdDetOpt.get());
            save(size);
        }
    }

    // @Override
    // public void save(Size model) {
    // System.out.println("Size service");
    // Optional<ProductDetails> lastProdDetOpt =
    // prodDetRepo.findById(model.getProductDetails().getId());
    // if (lastProdDetOpt.isPresent()) {
    // System.out.println(lastProdDetOpt.get().getId());
    // model.setProductDetails(lastProdDetOpt.get());
    // sizeRepository.save(model);
    // }
    // }

}
